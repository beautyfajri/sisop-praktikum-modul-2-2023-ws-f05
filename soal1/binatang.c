#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>

void download(char *url, char *namefile);
void unzip(char *sourceDir);
void getFile(char *directory);
void generateDirectory();
void moveAnimal(char *soruce, char *des1, char *des2, char *des3);
void zipAnimal();

int main(){
    download("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");
    unzip("binatang.zip");
    getFile(".");
    generateDirectory();
    moveAnimal(".", "HewanDarat", "HewanAir", "HewanAmphibi");
    zipAnimal();
}

void download(char *url, char *namefile){
    int stat;
    pid_t downloadID = fork();

    if (downloadID == 0){
    char *args[] = {"wget", "--no-check-certificate", url, "-q", "-O", namefile, NULL};
    execv("/opt/homebrew/bin/wget", args);
    }
    waitpid(downloadID, &stat, 0);
}

void unzip(char *sourceDir){
    int stat;
    pid_t unzipID = fork();

    if (unzipID == 0){
    char *args[] = {"unzip", "-q", sourceDir, NULL};
    execv("/usr/bin/unzip", args);
    }

    waitpid(unzipID, &stat, 0);
}

void getFile(char *directory){
    int stat;
  char *path[100];

  struct dirent *dp;
  DIR *folder;
    srand(time(NULL));
    folder = opendir(directory);

    if (folder != NULL){
    int i = 0;
    while ((dp = readdir(folder)) != NULL){
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".jpg") != NULL){
        if (dp->d_type == DT_REG){
          char *token = dp->d_name;
            path[i] = token;
            i++;
        }
        }
    }
    int size = sizeof(*path);
    int random = rand() % size;

    // Create file txt
    FILE *file;
    file = fopen("penjaga.txt", "w");
    fprintf(file, "Hewan Yang Dijaga : %s", strtok(path[random], "."));
    fclose(file);
    closedir(folder);
    }
}

void generateDirectory(){
    int stat;
    id_t child_id;

    if ((child_id = fork()) == 0){
    char *args[] = {"mkdir", "-p", "HewanDarat", NULL};
    execv("/bin/mkdir", args);
    }
    while ((wait(&stat)) > 0)
    ;

    if ((child_id = fork()) == 0){
    char *args[] = {"mkdir", "-p", "HewanAir", NULL};
    execv("/bin/mkdir", args);
    }
    while ((wait(&stat)) > 0)
    ;
    if ((child_id = fork()) == 0){
    char *args[] = {"mkdir", "-p", "HewanAmphibi", NULL};
    execv("/bin/mkdir", args);
    }
    while ((wait(&stat)) > 0)
    ;

    waitpid(child_id, &stat, 0);
}

void moveAnimal(char *soruce, char *des1, char *des2, char *des3){
    int stat;
    id_t child_id;
  struct dirent *dp;
  DIR *folder;
    folder = opendir(soruce);

    if (folder != NULL){
    while ((dp = readdir(folder)) != NULL){
        if (dp->d_type == DT_REG)
        {
        // if match with darat
        if (strstr(dp->d_name, "darat") != NULL){
            if ((child_id = fork()) == 0){
            char *argv[] = {"mv", dp->d_name, des1, NULL};
            execv("/bin/mv", argv);
            }
            while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp->d_name, "air") != NULL){
            if ((child_id = fork()) == 0){
            char *argv[] = {"mv", dp->d_name, des2, NULL};
            execv("/bin/mv", argv);
            }
            while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp->d_name, "amphibi") != NULL){
            if ((child_id = fork()) == 0){
            char *argv[] = {"mv", dp->d_name, des3, NULL};
            execv("/bin/mv", argv);
            }
            while ((wait(&stat)) > 0)
            ;
        }
        }
    }
    closedir(folder);
    }
    waitpid(child_id, &stat, 0);
}

void zipAnimal(){
    id_t child_id;
    int stat;
    if ((child_id = fork()) == 0){
    char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
    execv("/usr/bin/zip", argv);
    }
    while ((wait(&stat)) > 0)
    ;
    if ((child_id = fork()) == 0){
    char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
    execv("/usr/bin/zip", argv);
    }
    while ((wait(&stat)) > 0)
    ;
    if ((child_id = fork()) == 0){
    char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
    execv("/usr/bin/zip", argv);
    }
    while ((wait(&stat)) > 0)
    ;
    waitpid(child_id, &stat, 0);
}