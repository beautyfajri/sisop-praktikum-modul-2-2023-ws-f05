#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

char* make_dir(){
    char *now = malloc(sizeof(char) * 20);
    time_t t = time(NULL);
    strftime(now, 20, "%Y-%m-%d_%H:%M:%S", localtime(&t));//buat nama direk, sesuai peraturan
    pid_t child = fork();
    if(child == 0){
        char *cmd[] = {"mkdir", now, NULL};
        execv("/bin/mkdir", cmd);//execute mkdir
    }
    while(wait(NULL) != child);
    return now;//return nama dir
}

void start_program(char *dir) {
    char filename[64];
    char url[64];
    char location[100];
    for(int i = 0; i < 15; i++){
        pid_t child = fork();
        if(child == 0){
            time_t t = time(NULL);  // dapatkan waktu terkini dalam detik, Epoch
            long size = (long)t % 1000 + 50;  // hitung besar gambar
            strftime(filename, 64, "%Y-%m-%d_%H:%M:%S.png", localtime(&t));//filename
            sprintf(location, "%s/%s", dir, filename);  // spesifikasi output
            sprintf(url, "https://picsum.photos/%ld", size);//dapatkan url, dari perhitungan
            char *cmd[] = {"wget", "-q", "-O", location, url, NULL};
            execv("/opt/homebrew/bin/wget", cmd);//execute wget
        }
        sleep(5);//jeda 5 detik
    }
    while(wait(NULL) > 0);
    char zipName[30]; //nama zip
    sprintf(zipName, "%s.zip", dir);//masukkan dir menajdi nama zip
    pid_t child = fork();//child
    if(child == 0){
        char *cmd1[] = {"zip", "-r", zipName, dir, NULL};
        execv("/usr/bin/zip", cmd1);//execute zip -r
    }
    while(wait(NULL) != child);//wait
    char *cmd2[] = {"rm", "-r", dir, NULL};
    execv("/bin/rm", cmd2);//execute rm -r
}

void generate_killer(char *argv[], pid_t pid, pid_t sid) {
    FILE *fp = fopen("killer.c", "w");//buat file killer.c
    // Killer.c
    const char *command = ""
    "#include <unistd.h>\n"
    "#include <sys/wait.h>\n"
    "int main() {\n"
        "pid_t child = fork();\n"
        "if (child == 0) {\n"
            "%s"
            "execv(\"%s\", argv);\n"
        "}\n"
        "while(wait(NULL) > 0);\n"
        "child = fork();\n"
        "if (child == 0) {\n"
        "char *argv[] = {\"rm\", \"killer.c\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"//hapus killer.c
        "}\n"
        "while(wait(NULL) > 0);\n"
        "char *argv[] = {\"rm\", \"killer\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"//self destruct
    "}\n";
    // Mode A
    char cmd[1234];
    if (strcmp(argv[1], "-a") == 0) {
        sprintf(cmd, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
        fprintf(fp, command, cmd, "/usr/bin/pkill");//jika argumen -a, pkill
    }
    // Mode B
    if (strcmp(argv[1], "-b") == 0) {
        sprintf(cmd, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
        fprintf(fp, command, cmd, "/bin/kill");//jika argumen -b, kill
    }
    fclose(fp);
    // Compile killer.c
    pid = fork();
    if(pid == 0){    
        char *cmd[] = {"gcc", "killer.c", "-o", "killer", NULL};
        execv("/usr/bin/gcc", cmd);//execute gcc, compile killer.c
    }
    while(wait(NULL) != pid);
}
void daemonize(char *argv[]){
    pid_t sid, pid = fork();//seperti modul
    if(pid < 0) {
    exit(EXIT_FAILURE);
    }
    if(pid > 0) {
    exit(EXIT_SUCCESS);
    }
    umask(0);
    sid = setsid();
    if(sid < 0) {
    exit(EXIT_FAILURE);
    }
    generate_killer(argv, pid, sid);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s [-a|-b]\n", argv[0]);
        exit(1);
    }//jaga argumen
    daemonize(argv);//daemon
    while (1) {
        char *dir = make_dir();
        pid_t child = fork();
        if (child == 0) {
            start_program(dir);
        }
      sleep(30);//jeda 30 detik
    }
}

