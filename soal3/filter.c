#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>

void download(char *url, char *namefile);
void unzip(char *sourceDir);
void removeNotMU(char *directory);
void moveByRole();
void deletePlayerFolder();
void buatTim(int bek, int gelandang, int penyerang);

int main()
{
  /**
   * Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola.
   * Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”.
   * Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
   */
  download("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "players.zip");
  unzip("players.zip");

  /**
   * Dikarenakan database yang diunduh masih data mentah.
   * Maka bantulah Ten Hag untuk menghapus semua pemain yang
   * bukan dari Manchester United yang ada di directory.
   */
  removeNotMU("players");

  /**
   * Setelah mengetahui nama-nama pemain Manchester United,
   * Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan
   * posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda.
   * Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
   */
  moveByRole();
  deletePlayerFolder();

  /**
   * Setelah mengkategorikan anggota tim Manchester United,
   * Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU
   * berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang,
   * dan penyerang. (Kiper pasti satu pemain).
   * Untuk output nya akan menjadi
   * Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
   */
  printf("Masukkan jumlah bek, gelandang, dan penyerang: \n");
  int a, b, c;
  scanf("%d %d %d", &a, &b, &c);
  buatTim(a, b, c);

  printf("Selesai\n");


  return 0;
}

void download(char *url, char *namefile){
  int stat;
  pid_t downloadID = fork();

  if (downloadID == 0)
  {
    char *args[] = {"wget", "--no-check-certificate", url, "-O", namefile, NULL};
    // ! Note: /urs/local/bin/wget is the path to wget in MacOS change it to /usr/bin/wget if you are using Linux
    execv("/opt/homebrew/bin/wget", args);
  }
  waitpid(downloadID, &stat, 0);
}

void unzip(char *sourceDir){
  int stat;
  pid_t unzipID = fork();

  if (unzipID == 0)
  {
    char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }
  while (wait(&stat) > 0)
    ;

  unzipID = fork();
  if (unzipID == 0)
  {
    char *args[] = {"rm", "-f", sourceDir, NULL};
    execv("/bin/rm", args);
  }
  while (wait(&stat) > 0)
    ;

  waitpid(unzipID, &stat, 0);
}

void removeNotMU(char *directory){
  int stat;
  char *path;
  id_t child_id;
  struct dirent *dp;
  DIR *folder;
  folder = opendir(directory);

  if (folder != NULL)
  {
    int i = 0;
    while ((dp = readdir(folder)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)
      {
        if (dp->d_type == DT_REG)
        {
          if (strstr(dp->d_name, "ManUtd") == NULL)
          {
            path = malloc(strlen(directory) + strlen(dp->d_name) + 2);
            sprintf(path, "%s/%s", directory, dp->d_name);
            child_id = fork();
            if (child_id == 0)
            {
              char *args[] = {"rm", "-f", path, NULL};
              execv("/bin/rm", args);
            }
          }
        }
      }
    }
  }
  closedir(folder);
  waitpid(child_id, &stat, 0);
}

void moveByRole(){
  int stat;
  char *path;
  pid_t child_id[4];
  struct dirent *dp;
  DIR *folder;

  for (int i = 0; i < 4; i++)
  {
    char *newFolder;
    switch (i)
    {
    case 0:
      newFolder = "Bek";
      break;
    case 1:
      newFolder = "Gelandang";
      break;
    case 2:
      newFolder = "Kiper";
      break;
    case 3:
      newFolder = "Penyerang";
      break;
    }
    child_id[i] = fork();
    if (child_id[i] < 0)
    {
      printf("Gagal membuat child process.\n");
      exit(EXIT_FAILURE);
    }
    else if (child_id[i] == 0)
    {
      char *args[] = {"mkdir", "-p", newFolder, NULL};
      execv("/bin/mkdir", args);
    }

    while (wait(&stat) > 0)
      ;

    if (folder != NULL)
    {
      folder = opendir("players");
      while ((dp = readdir(folder)) != NULL)
      {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)
        {
          if (dp->d_type == DT_REG && strstr(dp->d_name, newFolder) != NULL)
          {
            path = malloc(strlen("players") + strlen(dp->d_name) + 2);
            sprintf(path, "%s/%s", "players", dp->d_name);
            child_id[i] = fork();
            if (child_id[i] == 0)
            {
              char *args[] = {"mv", "-f", path, newFolder, NULL};
              execv("/bin/mv", args);
            }
          }
        }
      }
    }
  }
  for (int i = 0; i < 4; i++)
  {
    waitpid(child_id[i], &stat, 0);
  }
  closedir(folder);
}

void deletePlayerFolder(){
  int stat;
  pid_t child_id = fork();
  if (child_id == 0)
  {
    char *args[] = {"rm", "-rf", "players", NULL};
    execv("/bin/rm", args);
  }
  waitpid(child_id, &stat, 0);
}

void buatTim(int bek, int gelandang, int penyerang){
  int stat;
  pid_t child_id;

  char args[200];
  child_id = fork();

  // Remove file if exist
  if (child_id == 0){
    snprintf(args, sizeof(args), "rm -f ~/Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }
  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Bek | sort -t _ -k4,4 -n -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", bek, bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }
  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Gelandang | sort -t _ -k4,4 -n -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", gelandang, bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }

  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Penyerang | sort -t _ -k4,4 -n -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", penyerang, bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }

  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Kiper | sort -t _ -k4,4 -n -r | head -n 1 >> ~/Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }

  waitpid(child_id, &stat, 0);
}
